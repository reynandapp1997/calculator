const products = [{
	name: "Sabun",
	price: null
}, {
	name: "Pena",
	price: 2000
}, {
	name: "Spidol",
	price: 5000
}];

const peoples = [{
	name: "Reynanda",
	address: "Batam"
}, {
	name: "Reza",
	address: "Batam"
}, {
	name: "Sam",
	address: "Medan"
}, {
	name: "Ken",
	address: "Batam"
}, {
	name: "Wiliam",
	address: "Batam"
}, ];

module.exports = {
	products,
	peoples
};