const filterHarga = (arr, price) => {
	arr.forEach(el => {
		if (el.price < price && el.price != null && el.name != null) {
			console.log(el);
		}
	})
};

const hiPeople = (arr) => {
	arr.forEach(el => {
		console.log(`Hi ${el.name}`);
	})
};

const byePeople = (arr) => {
	arr.forEach(el => {
		console.log(`Bye ${el.name}, going home to ${el.address}`);
	})
};

const calculateAge = (arr) => {
	arr.forEach(el => {
		console.log(`${el.name} age is ${new Date().getFullYear() - el.birthYear}`)
	})
}

module.exports = {
	filterHarga,
	hiPeople,
	byePeople,
	calculateAge
};