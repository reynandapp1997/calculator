const express = require('express');
const app = express();

app.get('/test', (req, res, next) => {
    res.status(200).json({
        message: 'test worked'
    });
});

app.get('/user/:user', (req, res, next) => {
    res.status(200).json({
        user: req.params.user
    });
});

app.get('/tambah/:first/:second', (req, res, next) => {
    res.status(200).json({
        result: parseInt(req.params.first) + parseInt(req.params.second)
    });
});

app.listen(8080, err => {
    if (err) {
        return console.log(err);
    }
    console.log('Listening on port 8080')
});

// class Person {
//     constructor(firstName, lastName, dateOfBirth) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//         this.dateOfBirth = new Date(dateOfBirth);
//     }
    
//     get fullName () {
//         return `${this.firstName} ${this.lastName}`
//     }

//     set setFirstName (firstName) {
//         this.firstName = firstName;
//     }

//     set setLastName (lastName) {
//         this.lastName = lastName;
//     }

//     set setDateOfBirth (dateOfBirth) {
//         this.dateOfBirth = new Date(dateOfBirth);
//     }

//     set favColor(color) {
//         this.color = color;
//     }
// }

// class Developer extends Person {
//     constructor(language) {
//         super();
//         this.language = language;
//     }
// }

// let rey = new Developer('Javascript');
// rey.setFirstName = 'Reynanda';
// rey.setLastName = 'Putra';
// rey.setDateOfBirth = new Date(1997, 2, 4);
// console.log(rey);
// console.log(rey.dateOfBirth.toLocaleDateString());

// const {
//     hiPeople,
//     byePeople,
//     calculateAge
// } = require('./function');

// class Person {
//     constructor(name, birthYear, address) {
//         this.name = name;
//         this.birthYear = birthYear;
//         this.address = address;
//     }

//     get whoAmI() {
//         return this.name;
//     }

//     set whoAmI(name) {
//         this.name = name;
//     }

//     calcAge() {
//         return new Date().getFullYear() - this.birthYear;
//     }

// }

// let people = [];

// let rey = new Person('Reynanda', 1997, 'Batam Center');
// let reza = new Person('Reza', 1998, 'Batu Aji');

// people.push(rey, reza);

// hiPeople(people);
// byePeople(people);
// via file function
// calculateAge(people);

// via class method
// console.log(rey.calcAge());
// console.log(reza.calcAge());

// const event = require('events');
// const express = require('express');

// const {
//     products,
//     peoples
// } = require('./data.js');
// const {
//     filterHarga,
//     hiPeople,
//     byePeople
// } = require('./function.js');

// const app = express();
// const PORT = 3001;

// app.get('/', (req, res, next) => {
//     // return res.send('Hello World');
//     return res.status(200).json({
//         message: 'Home'
//     });
// });

// app.listen(PORT || process.env.PORT, () => {
//     console.log(`Listening on PORT ${PORT}`)
// });

// const eventEmitter = new event.EventEmitter();

// eventEmitter.on('Hi', hiPeople);

// eventEmitter.emit('Hi', peoples);
// console.log('');
// eventEmitter.on('Bye', byePeople);

// eventEmitter.emit('Bye', peoples);

// filterHarga(products, 3000);

// const kabataku = (valOne, valTwo, method) => {
//     if (typeof valOne !== 'number' && typeof valTwo !== 'number') {
//         return console.log(`${valOne} dan ${valTwo} bukan type data number`)
//     } else if (typeof valOne !== 'number') {
//         return console.log(`${valOne} bukan type data number`)
//     } else if (typeof valTwo !== 'number') {
//         return console.log(`${valTwo} bukan type data number`)
//     }
//     switch (method) {
//         case 'kali':
//             console.log(valOne * valTwo)
//             break;
//         case 'bagi':
//             console.log(valOne / valTwo)
//             break;
//         case 'tambah':
//             console.log(valOne + valTwo)
//             break;
//         case 'kurang':
//             console.log(valOne - valTwo)
//             break;

//         default:
//             console.log('method tidak diketahui')
//             break;
//     }
// }
// kabataku(3, 2, 'kali')
// kabataku(3, 2, 'bagi')
// kabataku(3, 2, 'tambah')
// kabataku(3, 2, 'kurang')
// kabataku(3, 2, 'aa')

// console.log('Hello World')